# Use a imagem Node.js padrão
FROM node

# Informações de manutenção
LABEL maintainer="Felipe Auleon <auleonfelipe@gmail.com>"

# Define o diretório de trabalho dentro do contêiner
WORKDIR /app

# Copia o arquivo app.js para o diretório de trabalho
COPY ./app.js /app

# Instala o framework Express
RUN npm install express

# Comando padrão para executar a aplicação
ENTRYPOINT ["node", "app.js"]
