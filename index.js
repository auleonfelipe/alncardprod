// Importar o agente New Relic
require('newrelic');

const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const PORT = process.env.PORT || 3000;

// Middleware para fazer o parse do corpo da requisição como JSON
app.use(bodyParser.json());

// Rota para lidar com requisições POST para /webhook
app.post('/webhook', (req, res) => {
  // Aqui você pode adicionar a lógica para processar os dados recebidos no webhook
  console.log('Recebido webhook:');
  console.log(req.body); // O corpo da requisição contém os dados enviados

  // Responder ao cliente com uma mensagem simples
  res.status(200).send('Webhook recebido com sucesso!');
});

// Iniciar o servidor na porta especificada
app.listen(PORT, () => {
  console.log(`Servidor rodando na porta ${PORT}`);
});
